"""
Naive O(m^2) implementation of suffix tree
"""
from copy import deepcopy

def prefix_length(a, b):
    count = 0
    for x, y in zip(a, b):
        if x != y: break
        count += 1
    return count

class SuffixTree:
    def __init__(self):
        self.edges = dict()

    def add_edge(self, label, tree=None):
        self.edges[label] = tree if tree else SuffixTree()

    def remove_edge(self, label):
        if label in self.edges:
            self.edges.pop(label)

    def add(self, suffix):
        for label, edge in self.edges.items():
            l = prefix_length(label, suffix)
            if l:
                if l == len(label): # if a node is reached
                    edge.add(suffix[l:])

                elif 0 < l < len(label): # split the edge
                    self.add_edge(label[:l])
                    self.remove_edge(label)
                    self.edges[label[:l]].add_edge(label[l:], deepcopy(edge))
                    self.edges[label[:l]].add_edge(suffix[l:])
                return
        self.add_edge(suffix)

    def insert(self, word):
        for i in range(len(word)):
            self.add(word[i:])

    def __repr__(self):
        return repr(self.edges)

