"""
The stable marriage problem from CodeChef
"""
from collections import defaultdict

def revdict(dic):
    for key in dic.keys():
        dic[key] = dic[key][::-1]
    return dic

def smp(boy_prefs, girl_prefs):
    stable = {}

    ranks = dict()
    for girl, gpref in girl_prefs.items():
        for i, boy in enumerate(gpref):
            ranks[(girl, boy)] = i

    free_boys = set(boy_prefs.keys())
    free_girls = set(girl_prefs.keys())

    n = len(boy_prefs)
    # Reverse orders
    boy_prefs = revdict(boy_prefs)
    while free_boys:
        boy = free_boys.pop()
        if not boy_prefs.has_key(boy):
            continue
        girl = boy_prefs[boy].pop()
        if girl in free_girls:
            stable[girl] = (boy, ranks[(girl, boy)])
            free_girls.remove(girl)
        elif ranks[(girl, boy)] < stable[girl][1]:
            free_boys.add(stable[girl][0])
            stable[girl] = (boy, ranks[(girl, boy)])
        else:
            free_boys.add(boy)

    results = sorted((boy, girl) for girl, (boy, rank) in stable.items())
    return results    

def test():
    gprefs = { 1: [4, 3, 1, 2],
               2: [2, 1, 3, 4],
               3: [1, 3, 4, 2],
               4: [4, 3, 1, 2]
               }

    bprefs = {
        1: [3, 2, 4, 1],
        2: [2, 3, 1, 4],
        3: [3, 1, 2, 4],
        4: [3, 2, 4, 1]
    }

    print smp(bprefs, gprefs)

def main():
    cases = int(raw_input())
    for i in range(cases):
        k = int(raw_input())
        gprefs, bprefs = {}, {}
        for a in range(k):
            gprefs[a+1] = map(int, raw_input().split())[1:]
        for b in range(k):
            bprefs[b+1] = map(int, raw_input().split())[1:]
        for boy, girl in smp(bprefs, gprefs):
            print boy, girl

if __name__ == '__main__':
    main()

