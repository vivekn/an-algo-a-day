"""
Krager's randomized contraction algorithm
for finding the min-cut of a graph.
"""
from copy import deepcopy, copy
from random import choice
from collections import defaultdict

def random_cut(graph_old):
    supernodes = dict((x, [x]) for x in graph_old)
    graph = copy(graph_old)
    while len(graph) > 2:
        # Pick a random edge.
        v1 = choice(graph.keys())
        v2 = choice(graph[v1])

        # Remove self loops.
        graph[v1] = [x for x in graph[v1] if x != v2]
        graph[v2] = [x for x in graph[v2] if x != v1]
        graph[v1].extend(graph[v2])

        # Resolve pointers
        for vertex in graph[v2]:
            graph[vertex] = [x if x!=v2 else v1 for x in graph[vertex]]
        graph.pop(v2)

        # Combine supernodes
        supernodes[v1].extend(supernodes[v2])
        supernodes.pop(v2)
    return map(sorted, supernodes.values()), len(graph.values()[0])



def min_cut(graph):
    mincost = 10 ** 10
    mincut = []
    for i in range(len(graph) ** 2):
        cut, cost = random_cut(graph)
        if cost < mincost:
            mincut = cut
            mincost = cost
    return mincut, mincost

def test():
    graph = {
        1: [2,3,4],
        2: [1,3],
        3: [1,2,4],
        4: [1, 3]
    }
    print min_cut(graph)

def coursera():
    lines = open('kargerAdj.txt')
    graph = defaultdict(list)
    for line in lines:
        args = line.strip().split()
        graph[args[0]] = args[1:]
    print min_cut(graph)


if __name__ == '__main__':
    coursera()