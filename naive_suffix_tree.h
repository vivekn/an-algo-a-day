/*
 * Naive implementation of suffix tree, Quadratic preprocessing time, linear search time
 */ 

#include <map>
#include <string>
#include <algorithm>

using namespace std;

int prefix_length(string a, string b) {
    int i;
    for(i = 0; i < min(a.length(), b.length()); i++) 
        if (a[i] != b[i]) break;
    return i;
}

class SuffixTree {
    public:
        map<string, SuffixTree> edges;
        
        SuffixTree();
        SuffixTree(map<string, SuffixTree>);
        void add(string);
        void insert(string);
};

SuffixTree::SuffixTree (map<string, SuffixTree> new_edges) {
    edges = new_edges;
}

void SuffixTree::add(string suffix) {
    map<string, SuffixTree>::iterator iter;
    for (iter = edges.begin(); iter != edges.end(); iter++) {
        string label = iter->first;
        int l = prefix_length(label, suffix);
        if (l) {
            if (l == label.length()) 
                iter->second.add(suffix.substr(l));
            else { // Split the edge
                edges[label.substr(0, l)] = SuffixTree(); // Add the common prefix as an edge
                edges.erase(label); // Remove the old edge
                edges[label.substr(0, l)].edges[label.substr(l)] = SuffixTree(edges);
                edges[iter->first.substr(0, l)].edges[suffix.substr(l)] = SuffixTree();
            }
            return;
        }
    }
    edges[suffix] = SuffixTree();
}

void SuffixTree::insert(string word) {
    for (int i = 0; i < word.length(); i++) 
        add(word.substr(i));
}
