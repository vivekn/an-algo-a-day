"""
Breadth first search.
"""
from collections import deque

def bfs(graph, start):
    """
    BFS traversal
    """
    Q = deque([start])
    seen = set()
    while Q:
        u = Q.popleft()
        for v in graph[u]:
            if v not in seen:
                seen.add(v)
                Q.append(v)
    return seen

def bfs_distances(graph, start):
    Q = deque([start])
    seen = set()
    distances = {start:0}
    while Q:
        u = Q.popleft()
        for v in graph[u]:
            if v not in seen:
                distances[v] = distances[u] + 1
                seen.add(v)
                Q.append(v)
    return distances

def connected_components(graph):
    components = []
    explored = set() 
    for vertex in graph:
        if vertex not in explored:
            comp = bfs(vertex)
            explored |= bfs
            components.append(comp)
    return components


